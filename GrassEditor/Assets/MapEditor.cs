﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class MapEditor : MonoBehaviour {

	public static MapEditor s_Instance = null;

	public GameObject m_preGrassTile;
	public GameObject m_preGridLine;

	public GameObject m_goGridLineContainer;
	public GameObject m_goGrassContainer;

	const int c_GridNum = 1024;

	Dictionary<string, GrassTile> m_dictGrass = new Dictionary<string, GrassTile> ();
	Vector3 vecTempPos = new Vector3();

	float m_fGrassTileWidth = 2.46f;
	float m_fGrassTileHeight = 2.46f;
	float m_fGrassHalfTileWidth = 0.0f;
	float m_fGrassHalfTileHeight = 2.46f;
	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		s_Instance = this;

		m_fGrassHalfTileWidth = m_fGrassTileWidth / 2.0f;
		m_fGrassHalfTileHeight = m_fGrassTileHeight / 2.0f;

		InitGrid ();
	}

	// Update is called once per frame
	void Update () {
		ProcessInput ();
	}

	void InitGrid()
	{
		for (int i = -c_GridNum; i < c_GridNum; i++) {
				GameObject goLine = GameObject.Instantiate (m_preGridLine);
				goLine.transform.parent = m_goGridLineContainer.transform;
				LineRenderer lr = goLine.GetComponent<LineRenderer>();
				vecTempPos.x = m_fGrassTileWidth * (i + 0.5f);
				vecTempPos.y = -10000.0f;
				lr.SetPosition ( 0, vecTempPos );
				vecTempPos.y = 10000.0f;
				lr.SetPosition ( 1, vecTempPos );
		}

		for (int i = -c_GridNum; i < c_GridNum; i++) {
			GameObject goLine = GameObject.Instantiate (m_preGridLine);
			goLine.transform.parent = m_goGridLineContainer.transform;
			LineRenderer lr = goLine.GetComponent<LineRenderer>();
			vecTempPos.y = m_fGrassTileHeight * (i + 0.5f);
			vecTempPos.x = -10000.0f;
			lr.SetPosition ( 0, vecTempPos );
			vecTempPos.x = 10000.0f;
			lr.SetPosition ( 1, vecTempPos );
		}
	}

	void ProcessInput()
	{
		if (IsPointerOverUI ()) {
			return;
		}

		ProcessMouseWheel ();

		ProcessDragCamera ();

		if (Input.GetMouseButtonDown (0)) {
			vecTempPos = Camera.main.ScreenToWorldPoint( Input.mousePosition ) ;
			float x = 0.0f, y = 0.0f;
			if (vecTempPos.x > 0.0f) {
				x = vecTempPos.x + m_fGrassHalfTileWidth;
			} else if (vecTempPos.x < 0.0f) {
				x = vecTempPos.x - m_fGrassHalfTileWidth;
			}

			if (vecTempPos.y > 0.0f) {
				y = vecTempPos.y + m_fGrassHalfTileHeight;
			} else if (vecTempPos.y < 0.0f) {
				y = vecTempPos.y - m_fGrassHalfTileHeight;
			}

			int nX = (int)(x / m_fGrassTileWidth);
			int nY = (int)(y / m_fGrassTileHeight);

			GrassTile grass = GetGrass( nX, nY );
			if (grass == null) {
				grass = CreateGrassTile ( nX, nY );
				SetGrass ( grass, nX, nY );
			} else {
				RemoveGrass ( grass, nX, nY );
			}

		} // end if (Input.GetMouseButtonDown (0))
	}

	public GrassTile GetGrass( int nX, int nY )
	{
		GrassTile grass = null;
		string key = nX + "," + nY;
		m_dictGrass.TryGetValue (key, out grass);
		return grass;
	}

	void SetGrass( GrassTile grass, int nX, int nY   )
	{
		string key = nX + "," + nY;
		m_dictGrass[key] = grass;
	}

	void RemoveGrass( GrassTile grass, int nX, int nY )
	{
		if (grass != null) {
			grass.Die ();
		}

		string key = nX + "," + nY;
		m_dictGrass[key] = null;
	}

	GrassTile CreateGrassTile( int nX, int nY )
	{
		GrassTile grass = GameObject.Instantiate (m_preGrassTile).GetComponent<GrassTile> ();
		vecTempPos.x = m_fGrassTileWidth * nX;	
		vecTempPos.y = m_fGrassTileHeight * nY;
		grass.transform.localPosition = vecTempPos;
		grass.transform.parent = m_goGrassContainer.transform;
		grass.SetIndex ( nX, nY );
		grass.BindNeighbor (); // 注意，一定要先SetIndex()之后才能BindNeighbor(),否则没有意义。所以BindNeighbor()也没法放在Awake()里面作
		string key = nX + "," + nY;
		m_dictGrass [key] = grass;
		return grass;
	}

	void ProcessMouseWheel()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) {  
			Camera.main.orthographicSize += 1.0f;//Time.deltaTime * 1.0f;
		} else if (Input.GetAxis ("Mouse ScrollWheel") >0) {
			Camera.main.orthographicSize -= 1.0f;//Time.deltaTime * 1.0f;
		}
	}

	bool m_bFirstMouseRightDown = true;
	Vector3 m_vecLastMousePos = new Vector3();
	void ProcessDragCamera ()
	{
		if (Input.GetMouseButton(1)) {
			vecTempPos = Camera.main.ScreenToWorldPoint ( Input.mousePosition );
			if (m_bFirstMouseRightDown == true) {
				m_vecLastMousePos = vecTempPos;
				m_bFirstMouseRightDown = false;
				return;
			}
			Vector3 vecDelta = vecTempPos - m_vecLastMousePos;
			Camera.main.transform.position -= vecDelta / 2.0f;
			m_vecLastMousePos = vecTempPos;
		} else {
			m_bFirstMouseRightDown = true;
		}
	}


	// 判断当前是否点击在了UI上
	public bool IsPointerOverUI()
	{

		PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = Input.mousePosition;
		eventData.position = Input.mousePosition;
	
		List<RaycastResult> list = new List<RaycastResult>();
		UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);
	
		return list.Count > 0;

	}

	public void Save()
	{
		XmlDocument xmlDoc = new XmlDocument();  


		//创建类型声明节点  
		XmlNode node=xmlDoc.CreateXmlDeclaration("1.0","utf-8","");  
		xmlDoc.AppendChild(node);  
		//创建根节点  
		XmlNode root = xmlDoc.CreateElement("root");  
		xmlDoc.AppendChild(root);  
		foreach (KeyValuePair<string, GrassTile> pair in m_dictGrass) {
			CreateNode(xmlDoc, root, "P", pair.Key);  
		}

		//保存
		xmlDoc.Save("test.xml");
	}
	public void CreateNode(XmlDocument xmlDoc,XmlNode parentNode,string name,string value)  
	{  
		XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);  
		node.InnerText = value;  
		parentNode.AppendChild(node);  
	}  
	public void Load()
	{
		XmlDocument myXmlDoc = new XmlDocument();
		myXmlDoc.Load("test.xml");
		XmlNode rootNode = myXmlDoc.SelectSingleNode("root");
		for (int i = 0; i < rootNode.ChildNodes.Count; i++) {
			XmlNode node = rootNode.ChildNodes [i];
			string key = node.InnerText;
			string[] ary = key.Split ( ',' );
			int nX = int.Parse( ary [0] );
			int nY = int.Parse( ary [1] );
			CreateGrassTile ( nX, nY );
		}
			
	}

	public static Dictionary<string, GrassTile> s_dicTempSetAlpha = new Dictionary<string, GrassTile> ();
	public void ProcessPlayerEnterGrass( GrassTile grass )
	{
		s_dicTempSetAlpha.Clear ();
		grass.SetAlpha ( 0.4f );
	}

	public  void ProcessPlayerExitGrass( GrassTile grass )
	{
		s_dicTempSetAlpha.Clear ();
		grass.SetAlpha ( 1.0f );
	}
}
