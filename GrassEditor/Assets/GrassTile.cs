﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassTile : MonoBehaviour {

	Color cTempColor = new Color();

	public SpriteRenderer _srMain;

	List<GrassTile> m_lstNeighbors = new List<GrassTile>(); // 相邻的草皮
	int m_nX = 0; // 在地图上的格子编号
	int m_nY = 0;

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{

	}

	// Update is called once per frame
	void Update () {
		
	}

	// 草皮消亡
	public void Die()
	{
		UnBindNeighbor ();

		GameObject.Destroy ( this.gameObject );
	}
		
	// 侦测并绑定相邻的草皮
	public void BindNeighbor()
	{
		// 按“九宫格”来检索。一个草皮周围的8个草皮都是它的相邻
		/*
		 *             7     0     1
		 *             6   self    2
		 *             5     4     3
		 */
		int nX = 0, nY = 0;
		for( int i = 0; i < 8; i++ )
		{
			switch(i)
			{ 
			case 0:
				{
					nX = m_nX;
					nY = m_nY + 1;
				}
				break;
			case 1:
				{
					nX = m_nX + 1;
					nY = m_nY + 1;
				}
				break;
			case 2:
				{
					nX = m_nX + 1;
					nY = m_nY;
				}
				break;
			case 3:
				{
					nX = m_nX + 1;
					nY = m_nY - 1;
				}
				break;
			case 4:
				{
					nX = m_nX;
					nY = m_nY - 1;
				}
				break;
			case 5:
				{
					nX = m_nX - 1;
					nY = m_nY - 1;
				}
				break;
			case 6:
				{
					nX = m_nX - 1;
					nY = m_nY;
				}
				break;
			case 7:
				{
					nX = m_nX - 1;
					nY = m_nY + 1;
				}
				break;

			} // end switch

			GrassTile grass = MapEditor.s_Instance.GetGrass ( nX, nY );
			if (grass == null) {
				continue;
			}
			this.AddNeighbor (grass);
			grass.AddNeighbor (this);

		} // end foer
	}

	// 解绑相邻的草皮
	void UnBindNeighbor()
	{

	}

	public void SetIndex( int nX, int nY )
	{
		m_nX = nX;
		m_nY = nY;
	}

	public void GetIndex( ref int nX, ref int nY )
	{
		nX = m_nX;
		nY = m_nY;
	}

	public void AddNeighbor( GrassTile grass )
	{
		m_lstNeighbors.Add ( grass );
	}

	public void RemoveNeighbor( GrassTile grass )
	{
		m_lstNeighbors.Remove ( grass );
	}

	/*
	 * 这是一个递归函数，不但本草皮要执行，跟它相关联的所有临近草皮都要执行
	 */
	public void SetAlpha( float fAlpha )
	{
		GrassTile grass = null;
		string key = m_nX + "," + m_nY;
		if (MapEditor.s_dicTempSetAlpha.TryGetValue (key, out grass)) {
			return;
		}
			
		cTempColor = _srMain.color;
		cTempColor.a = fAlpha;
		_srMain.color = cTempColor;

		int nX = 0, nY = 0;
		GetIndex ( ref nX, ref nY );
		key = nX + "," + nY;
		MapEditor.s_dicTempSetAlpha [key] = this;

		for (int i = 0; i < m_lstNeighbors.Count; i++) {
			grass = m_lstNeighbors[i];
			if (grass == null) {
				continue;
			}
			grass.SetAlpha ( fAlpha );
		}
	}
}
