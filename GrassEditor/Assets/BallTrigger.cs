﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 接触

	void OnTriggerEnter2D(Collider2D other)
	{
		GrassTile grass = other.transform.gameObject.GetComponent<GrassTile> ();
		if (grass) {
			MapEditor.s_Instance.ProcessPlayerEnterGrass (grass);
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		
	}

	void OnTriggerExit2D(Collider2D other)
	{
		GrassTile grass = other.transform.gameObject.GetComponent<GrassTile> ();
		if (grass) {
			MapEditor.s_Instance.ProcessPlayerExitGrass ( grass );
		}
	}
}
